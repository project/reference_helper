/**
 * @file
 * Javascript to auto-populate entity reference fields with recommended entities.
 */
(function($) {
  
  Drupal.behaviors.referenceHelper = {
    
    attach : function() {
      $('a.reference-helper').click(function() {
        var $target = $(this).parents('.references-helper').siblings('.form-item:last').find('.references-helper-target');
        if($target.attr('type') == 'text') {
          $target.val($(this).text() + ' (' + $(this).data('entity') + ')');
        }
        else {
          $target.find('input[value=' + $(this).data('entity') + ']').attr('checked', 'checked');
        }
      });
    }
  };
})(jQuery);