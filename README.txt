=======================
Reference helper module
=======================

When using an entity reference field (i.e. a field that references another item like a node or user) on a site with thousands of pieces of content, authors can get overwhelmed with the amount of options they have. For example, an editor on a news website with news going back several years often needs to relate two stories together, but when they start typing "Cat saved from tree" into the autocomplete entity reference field, it's difficult to find the story they were just writing ten minutes ago, versus the story about feline heroicism from 2003.

The reference helper module allows site builders to attach a widget to the bottom of an entity reference field with a list of suggested entities. By default, the follow three "helpers" are provided with the module:

* Show recent selections by the current user - When a user goes to add a value to an entity reference field, they are shown all the previous values that they used in the past. This is great when a user might need to reference an entity several times in a row.
* Show recent selections by all users - Displays a list of all the most recent values for the given field by all users, useful to show recently created or referenced fields regardless of which user inserted the value.
* Most used entities in this field - Looks at all the entities referenced in the current field and selects the most referenced ones. Useful for popular posts.

Writing your own helper
-----------------------

If you'd like to write your own helper to find suggested references, check out the reference_helper.api.inc file, as well as the provided example module called reference_helper_readability.

Hooks
-----

There are two hooks provided by this module:

hook_reference_helper_info :

  Should return a list of helpers the current module provides. Your array should be in the form of:
  
  callback_name :
   label: A human-readable label (for the field settings form)
   file: (optional) the file where the callback lives
   form: (optional)Additional form elements to configure this helper
   
  The callback_name should be the name of a function which will recieve the entire context of the field and return an array of suggested items. That array should be in the form of:
  
  entity_id:
    label: The human-readable name of the entity (i.e. node title)
    title: (optional) An explanation of why this item was suggested
    
hook_reference_helper_alter(&$list, $context):

This hook allows a module to change the list of items that are going to be suggested to users before it is displayed. The $list array is in the same format as referenced in the above callback return values.

To alter the values, just change the list items. You can also use the provided context argument to get information about the current form the user is on, including the entity (i.e. node) they are editing.
