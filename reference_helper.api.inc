<?php

/**
 * @file Example API hooks for Reference Helper.
 */


/**
 * Alter callback, allows you to alter the list of suggested
 * entites before they are rendered.
 * $list: The list of suggested items
 * $context: The full context of the field
 */
function hook_reference_helper_alter(&$list, $context) {
  
}

/**
 * Hook to return the type of reference heleprs this module will define.
 *  Return: an associative array where the key is the name of the callback, and the values are:
 *    label: A human-readable label (for the field settings form)
 *    file: (optional) the file where the callback lives
 *    form: Additional form elements to configure this helper
 */
function hook_reference_helper_info() {
  return array(
      'my_reference_helper_example' => array('label' => t('An example reference helper'),
        'file' => drupal_get_path('module', 'mymodule') . '/mymodule.inc',
        'form' => 'my_reference_helper_form'),
    );
}

/**
 * An example form callback for the above reference helper.
 */
function my_reference_helper_form($settings) {
  $form = array();
  
  $form['order_by'] = array(
    '#type' => 'select',
    '#title' => t('Order by'),
    '#options' => array('created' => t('Created date'),
                        'updated' => t('Updated date'),
                        ),
    '#default_value' => (isset($settings['order_by'])) ? $settings['order_by'] : 'created',
  );
  
  return $form;
}

/**
 * An example callback function. It is passed the entire context of the field,
 * which includes settings and field configuration.
 * Return: a list of all the items to appear as suggestions, an associative array where:
 *    The key is the entity ID
 *    label: The name or title of the entity
 *    title: Any message that explains why this entity is in the list 
 */
function my_reference_helper_example($context) {
  return array(1 => array('label' => 'Node one',
                     'title' => 'This is hard-coded in my module'),
          2 => array('label' => 'Node two',
                     'title' => 'This is also hard-coded in my module'),
          );

}