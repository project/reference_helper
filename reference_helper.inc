<?php

/**
 * @file
 * The built-in helpers that come with the Reference helper module are stored here,
 * and included dynamically because this file was in the 'file' attribute used in
 * reference_helper_refererence_helper_info().
 */
 
/**
 * Reference helper to select the most used references in the given field.
 * Note that this will only work with fields stored in SQL databases (most of the time),
 * so we just return nothing if that's not the case.
 */
function reference_helper_most_used($context) {
  if ($context['field']['storage']['type'] != 'field_sql_storage') {
    return array();
  }
  $tables = $context['field']['storage']['details']['sql'][FIELD_LOAD_CURRENT];
  $table = array_shift(array_keys($tables));
  
  // Building the query dynamically so we can add additional expressions if needed
  $query = db_select($table, 't')
             ->fields('t', array('entity_type', 'bundle', $tables[$table]['target_id']))
             ->range(0, $context['instance']['reference_helper']['reference_helper_limit'])
             ->groupBy($tables[$table]['target_id'])
             ->orderBy('COUNT(t.' . $tables[$table]['target_id'] . ')', 'DESC');
  // If the entity reference field applies to all bundles of an entity (i.e. all nodes regardless of their type),
  // then this query is finished. However, if that's not the case, then we need to add more conditions
  // to the query. Note that we get the table that the entiy is stored in from that entity's info
  // by calling entity_get_info and using it's base table.
  if (count($context['field']['settings']['handler_settings']['target_bundles'])) {
    $entity_info = entity_get_info($context['field']['settings']['target_type']);
    $query->leftJoin($entity_info['base table'], 'e', 'e.' . $entity_info['entity keys']['id'] . ' = t.' . $tables[$table]['target_id']);
    $query->condition('e.' . $entity_info['bundle keys']['bundle'], $context['field']['settings']['handler_settings']['target_bundles']);
  }
  
  $query->addExpression('COUNT(t.' . $tables[$table]['target_id'] . ')', 'total');
  $result = $query->execute()
             ->fetchAll();
  // The entity reference module contains handlers for all the entity types that it supports,
  // and we use that handler to tell us what the label (i.e. user name for users, title for nodes)
  // should be used for any given entity.
  $handler = entityreference_get_selection_handler($context['field']);
  $list = array();
  foreach ($result as $row) {
    $entity = entity_load($context['field']['settings']['target_type'], array($row->{$tables[$table]['target_id']}));
    $entity = $entity[$row->{$tables[$table]['target_id']}];
    // The list should just be a list of arrays with a label and optional title attribute.
    // Check the examples in reference_helper.api.inc
    $list[$row->{$tables[$table]['target_id']}] = array('label' => $handler->getLabel($entity),
      'title' => format_plural($row->total,
                  'This item has been selected once.',
                  'This item has been selected @total times.',
                  array('@total' => $row->total)),
    );
  }
  return $list;
}

/**
 * Reference helper for using the most recent items selected in the current field by the current user.
 * This information is stored in the user's session, and is set using the insert_callback defined in
 * reference_helper_reference_helper_info().
 */
function reference_helper_recent($context) {
  if (!isset($_SESSION['reference_helper_recent'][$context['field']['field_name']])) {
    return;
  }
  $handler = entityreference_get_selection_handler($context['field']);
  $list = array();
  // Entity load accepts an array of entity IDs, which is what we store in our session variable.
  $entities = entity_load($context['field']['settings']['target_type'], $_SESSION['reference_helper_recent'][$context['field']['field_name']]);
  foreach ($entities as $id => $entity) {
    $list[$id] = array('label' => $handler->getLabel($entity));
  }
  return $list;  
}


/**
 * Reference helper for using the most recent items selected in the current field by all users.
 * Whenever any user references an entity, that is stored in a global variable. We pull those
 * entity IDs from the variable, load the entities, and then create a list of them.
 */
function reference_helper_recent_all($context) {
  $entities = variable_get('reference_helper_most_used', array($context['field']['field_name'] => array()));
  $handler = entityreference_get_selection_handler($context['field']);
  $list = array();
  $entities = entity_load($context['field']['settings']['target_type'], $entities[$context['field']['field_name']]);
  foreach ($entities as $id => $entity) {
    $list[$id] = array('label' => $handler->getLabel($entity));
  }
  return $list;  
}

/**
 * The insert callback for the recently used entity helper. Here we take
 * the element's information right after the field is saved, take the entity
 * ID out of the value (using regular expressions) and store it in a session.
 */
function reference_helper_recent_insert($element) {
  if (!isset($_SESSION['reference_helper_recent'][$element['#field_name']])) {
    $_SESSION['reference_helper_recent'][$element['#field_name']] = array();
  }
  
  $entities = $_SESSION['reference_helper_recent'][$element['#field_name']];
  $matches = array();
  // Entity reference fields are stored in the format of "entity label (id)", 
  // here we are pulling out the ID and storing it.
  if (preg_match("/.+\((\d+)\)/", $element['#value'], $matches)) {
    $value = $matches[1];
    $entities[$value] = $value;
  }
  if (count($entities) > $element['#reference_helper']['reference_helper_limit']) {
    $entities = array_pop(array_chunk($entities, $element['#reference_helper']['reference_helper_limit']));
  }
  $_SESSION['reference_helper_recent'][$element['#field_name']] = $entities;
}


/**
 * Insert callback for the "all recent values across all users" helper. This does the same
 * as the recent values for the current user helper, but instead stores
 * those values in a variable.
 * @todo - reference_helper_recent_insert() & reference_helper_recent_all_insert() are very similar
 * and should probably have the array building put into a single function.
 */
function reference_helper_recent_all_insert($element) {
  $entities = variable_get('reference_helper_most_used', array($element['#field_name'] => array()));
  $matches = array();
  if (preg_match("/.+\((\d+)\)/", $element['#value'], $matches)) {
    $value = $matches[1];
    $entities[$element['#field_name']][$value] = $value;
  }
  if (count($entities[$element['#field_name']]) > $element['#reference_helper']['reference_helper_limit']) {
    $entities = array_pop(array_chunk($entities[$element['#field_name']], $element['#reference_helper']['reference_helper_limit']));
  }
  variable_set('reference_helper_most_used', $entities);
}